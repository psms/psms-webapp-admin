# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template

import json

class TestView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin/testconfig', 'tests_config_getpage', self.tests_config_getpage, methods=['GET'])
        app.add_url_rule('/admin/testlist', 'tests_list', self.tests_list, methods=['GET'])
        app.add_url_rule('/admin/testanalyzer', 'test_analyzer', self.test_analyzer, methods=['GET'])

    def tests_config_getpage(self):
        return render_template('test_config.html')

    def tests_list(self):
        return render_template('test_list.html')

    def test_analyzer(self):
        return render_template('test_analyze.html')