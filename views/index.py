# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template
from flask import send_file

import json
import os

class IndexView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin', 'index', self.index, methods=['GET'])
        app.add_url_rule('/admin/logo', 'logo', self.logo, methods=['GET'])

    def index(self):
        return render_template('index.html')
        
    def logo(self):
        return send_file('{}/../static/img/logo.png'.format(os.path.dirname(os.path.abspath(__file__))), mimetype='image/png')