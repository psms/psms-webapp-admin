# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template

import json

class CategoriesView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin/categories/new', 'new_category_getpage', self.new_category_getpage, methods=['GET'])

    def new_category_getpage(self):
        return render_template('new_category.html')