# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template

import json

class DistributionsView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin/distribution', 'distribution_config_getpage', self.distribution_config_getpage, methods=['GET'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

    def distribution_config_getpage(self):
        return render_template('distribution_config.html')