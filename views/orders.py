# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template

import json

class OrdersView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin/orders', 'mange_orders_getpage', self.mange_orders_getpage, methods=['GET'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

    def mange_orders_getpage(self):
        return render_template('manage_orders.html')