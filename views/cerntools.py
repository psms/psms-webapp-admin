# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import render_template

import json
import ldap3

class CERNToolsView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/admin/user/search', 'search_cern_user', self.search_cern_user, methods=['POST'])

    def search_cern_user(self):
        data = request.json
        username = data['search']
        
        server = ldap3.Server("xldap.cern.ch")
        base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        conn = ldap3.Connection(server, auto_bind=True)
        search_filter = "(|(CN=" + username + "*)(userPrincipalName="+username+"*)(displayName="+username+"*))"
        conn.search(search_base=base_dn, search_filter=search_filter, attributes=["userPrincipalName","CN"])

        users = []
        for entry in conn.entries:
            users.append({
                'email': str(entry.userPrincipalName),
                'cn': str(entry.CN)
            })
            
        return json.dumps(users)