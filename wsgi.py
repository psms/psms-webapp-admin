# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

from flask import Flask

from gevent import pywsgi

import random
import string
import views.index as index
import views.parts as parts
import views.productions as productions
import views.categories as categories
import views.cerntools as cerntools
import views.distributions as distributions
import views.orders as orders
import views.tests as tests

import os
#Initialise FLASK Application
application = Flask(__name__)
application.config['TEMPLATES_AUTO_RELOAD'] = True

try:
    application.secret_key = os.environ['SECRET_KEY']
except:
    application.secret_key = ''.join(random.choice(string.ascii_letters + string.digits) for i in range (20))

indexview = index.IndexView(application)
partsview = parts.PartsView(application)
catview = categories.CategoriesView(application)
cerntoolsview = cerntools.CERNToolsView(application)
productionsview = productions.ProductionView(application)
distributionsview = distributions.DistributionsView(application)
ordersview = orders.OrdersView(application)
testview = tests.TestView(application)

if __name__ == "__main__":
    server = pywsgi.WSGIServer(('0.0.0.0', 2000), application)
    print('Start webserver..')
    server.serve_forever()
